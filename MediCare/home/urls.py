from django.urls import path
from . import views

app_name = "home"

urlpatterns = [
    path('', views.IndexView.as_view(), name="index"),
    path('menu', views.MenuView.as_view(), name="menu"),
    path('menuadmin', views.MenuAdminView.as_view(), name="menuadmin"),
    path('menualmacen', views.MenuAlmacenView.as_view(), name="menualmacen"),
    path('menualergias', views.MenuAlergiasView.as_view(), name="menualergias"),
    path('menuconsultas', views.MenuConsultasView.as_view(), name="menuconsultas"),
    path('menurecetas', views.MenuRecetasView.as_view(), name="menurecetas"),
    path('login/', views.LoginView.as_view(), name='login'),
    path('logout/', views.LogoutView, name='logout'),
]
