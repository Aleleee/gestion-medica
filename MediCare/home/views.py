from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect
from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import LoginForm
from django.contrib import messages

class IndexView(generic.View):
    template_name = "home/index.html"
    context = {}

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, self.context)

from django.contrib.auth import authenticate, login, logout
from django.contrib import messages  # Agrega esta línea
from django.shortcuts import render, redirect
from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import LoginForm

class LoginView(generic.View):
    template_name = 'home/login.html'

    def get(self, request):
        form = LoginForm()
        context = {'form': form}
        return render(request, self.template_name, context)

    def post(self, request):
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = authenticate(request, username=username, password=password)

            if user is not None:
                if user.is_staff:
                    login(request, user)
                    return redirect('home:menuadmin')
                
                if user.is_superuser:
                    login(request, user)
                    return redirect('home:menu')
                
        context = {'form': form}
        return render(request, self.template_name, context)


def LogoutView(request):
    logout(request)
    return redirect('home:index')

class MenuView(LoginRequiredMixin, generic.View):
    template_name = "home/menu.html"
    context = {}
    
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, self.context)

class MenuAlmacenView(LoginRequiredMixin, generic.View):
    template_name = "home/menualmacen.html"
    context = {}
    
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, self.context)
    
class MenuAlergiasView(LoginRequiredMixin, generic.View):
    template_name = "home/menualergias.html"
    context = {}
    
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, self.context)
    
class MenuConsultasView(LoginRequiredMixin, generic.View):
    template_name = "home/menuconsultas.html"
    context = {}
    
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, self.context)
    
class MenuRecetasView(LoginRequiredMixin, generic.View):
    template_name = "home/menurecetas.html"
    context = {}
    
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, self.context)
    
class MenuAdminView(LoginRequiredMixin, generic.View):
    template_name = "home/menuadmin.html"
    context = {}
    
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, self.context)